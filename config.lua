--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

-- general
lvim.log.level = "info"
lvim.format_on_save = false
lvim.colorscheme = "kanagawa"

-- vim options
vim.opt.timeoutlen = 500
vim.opt.wrap = true
vim.opt.clipboard = "unnamedplus"
vim.opt.mouse=""
vim.opt.title = false -- Fix a weird issue in Fedora 37 (https://github.com/LunarVim/LunarVim/issues/3481)
vim.opt.shiftwidth = 4;
vim.o.expandtab = false
vim.opt.tabstop = 4;
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- remove all trailing whitespaces from current file
lvim.builtin.which_key.mappings["k"] = { "<cmd>%s/\\s\\+$//e<cr>:noh<cr>", "Delete trailing whitespace from file" }
-- add your own keymapping
lvim.builtin.which_key.mappings["r"] = { "<cmd>HopPattern<cr>", "Hop to pattern" }

-- Toggle between dark and light mode
lvim.builtin.which_key.mappings["t"] = { ':exec &bg=="light"?"set bg=dark":"set bg=light"<cr>', "Toggle between dark and light mode" }
-- -- Auto load right theme
if os.getenv('theme') ~= '' then
  vim.o.background = os.getenv('theme')
end

lvim.builtin.which_key.mappings["F"] = { "<cmd>lua require('spectre').open()<cr>", "Find and replace" }

-- Enable Alt-L and Alt-H to move between buffers (https://github.com/LunarVim/LunarVim/commit/45f9825d1e666890ed37baf15a14707ae40e5cff)
lvim.keys.normal_mode["<M-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<M-h>"] = ":BufferLineCyclePrev<CR>"
lvim.builtin.which_key.mappings["j"] = { "<cmd>BufferLinePick<cr>", "Pick buffer" }

-- Close all notifications from Noice
lvim.builtin.which_key.mappings["x"] = { "<cmd>Noice dismiss<cr>", "Dismiss notifications" }

lvim.builtin.which_key.mappings["b"]["a"] = {"<cmd>bufdo bwipeout<cr>", "Close all buffers"}

-- Redeclare all Git keys bc I couldn't just delete gj and gk
lvim.builtin.which_key.mappings["g"] = {
  name = "Git",
  -- g = {"<cmd>GitUi<CR>", "GitUi"},
  g = {"<cmd>LazyGit<CR>", "LazyGit"},
  h = {"<cmd>LazyGitFilterCurrentFile<CR>", "LazyGit history"},
  e = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
  i = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
  l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
  n = { "<cmd>Neogit<cr>", "Neogit" },
  p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
  r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
  R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
  s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
  u = { "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>", "Undo Stage Hunk" },
  o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
  b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
  c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
  C = { "<cmd>Telescope git_bcommits<cr>", "Checkout commit(for current file)" },
  d = { "<cmd>Gitsigns diffthis HEAD<cr>", "Git Diff" }
}

lvim.builtin.which_key.mappings["y"] = {
  name = "Yank",
  h = { "<cmd>Telescope yank_history<cr>", "History" }
}

lvim.builtin.which_key.mappings["S"]= {
  name = "Session",
  c = { "<cmd>lua require('persistence').load()<cr>", "Restore last session for current dir" },
  l = { "<cmd>lua require('persistence').load({ last = true })<cr>", "Restore last session" },
  Q = { "<cmd>lua require('persistence').stop()<cr>", "Quit without saving session"},
}

-- vim-flutter keymappings
lvim.builtin.which_key.mappings["U"] = {
  name = "+Flutter",
  a = { "<cmd>FlutterRun<cr>", "Run" },
  q = { "<cmd>FlutterQuit<cr>", "Quit" },
  r = { "<cmd>FlutterReload<cr>", "Reload" },
  R = { "<cmd>FlutterRestart<cr>", "Restart" },
}

-- New file keymappings
lvim.builtin.which_key.mappings["n"] = {
  name = "+New buffer",
  n = { "<cmd>enew<cr>", "New tab" },
  o = { "<cmd>vs<cr>", "Split vertical" },
  e = { "<cmd>split<cr>", "Split horizontal" },
}

-- Focus windows
lvim.keys.normal_mode["<C-Up>"] = "<C-w>k"
lvim.keys.normal_mode["<C-Down>"] = "<C-w>j"
lvim.keys.normal_mode["<C-Left>"] = "<C-w>h"
lvim.keys.normal_mode["<C-Right>"] = "<C-w>l"

-- TODO: remover isso eventualmente para não conflitar com o TMUX
lvim.keys.normal_mode["<M-n>"] = "<C-w>h"
lvim.keys.normal_mode["<M-e>"] = "<C-w>j"
lvim.keys.normal_mode["<M-i>"] = "<C-w>k"
lvim.keys.normal_mode["<M-o>"] = "<C-w>l"

-- nvim-spider keybindings
vim.keymap.set({"n", "o", "x"}, "w", function() require("spider").motion("w") end, { desc = "Spider-w" })
vim.keymap.set({"n", "o", "x"}, "e", function() require("spider").motion("e") end, { desc = "Spider-e" })
vim.keymap.set({"n", "o", "x"}, "b", function() require("spider").motion("b") end, { desc = "Spider-b" })
vim.keymap.set({"n", "o", "x"}, "ge", function() require("spider").motion("ge") end, { desc = "Spider-ge" })

-- Insere quebra de linha sem sair do normal mode
lvim.keys.normal_mode["<Enter>"] = "o<Esc>"
lvim.keys.normal_mode["<M-Enter>"] = "O<Esc>"

-- Other vim settings
vim.g.vim_matchtag_enable_by_default = 1
vim.g.vim_matchtag_files = '*.html,*.xml,*.js,*.jsx,*.vue,*.svelte,*.jsp,*.tpl'
vim.g.VM_default_mappings = 0 -- Remove os keybindings default do vim-visual-multi para não conflitar com os meus

-- Better telescope behavior
local actions = require("telescope.actions")
lvim.builtin.telescope.defaults.mappings.i["<esc>"] = actions.close
lvim.builtin.telescope.pickers.buffers = {
  initial_mode = "insert"
}
lvim.builtin.telescope.pickers.live_grep = {
  layout_config = {
    width = 0.8
  }
}
-- Allow lsp to suggest from multiple sources
lvim.builtin.cmp.formatting.duplicates["vim_ls"] = 1

-- unmap a default keymapping
-- vim.keymap.del("n", "<C-Up>")
-- override a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>" -- or vim.keymap.set("n", "<C-q>", ":q<cr>" )

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
-- lvim.builtin.which_key.mappings["t"] = {
--   name = "+Trouble",
--   r = { "<cmd>Trouble lsp_references<cr>", "References" },
--   f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
--   d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
--   q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
--   l = { "<cmd>Trouble loclist<cr>", "LocationList" },
--   w = { "<cmd>Trouble workspace_diagnostics<cr>", "Wordspace Diagnostics" },
-- }

-- User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
-- lvim.builtin.notify.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true
lvim.builtin.breadcrumbs.active = true

-- Statusline config
lvim.builtin.lualine.style = "lvim"
local components = require("lvim.core.lualine.components")

lvim.builtin.lualine.sections.lualine_c = {
  components.filename,
  components.diff,
  components.python_env,
}
lvim.builtin.lualine.sections.lualine_x = {
  components.diagnostics,
  components.treesitter,
  components.lsp,
  components.spaces,
  components.filetype,
  "encoding",
}

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "dart",
  "fish",
  "html",
  "javascript",
  "json",
  "lua",
  "markdown_inline",
  "typescript",
  "tsx",
  "css",
  "perl",
  "regex",
  "scss",
  "rust",
  "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true
lvim.builtin.treesitter.rainbow.enable = true

-- generic LSP settings

-- Desabilita o destaque estranho de termos
lvim.lsp.document_highlight = false
-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "perlnavigator" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)
-- local pls_config = {
--   cmd = { 'perlo' }, -- complete path to where PLS is located
--   settings = {
--     pls = {
--       -- inc = { '/my/perl/5.34/lib', '/some/other/perl/lib' },  -- add list of dirs to @INC
--       -- cwd = { '/my/projects' },   -- working directory for PLS
--       perlcritic = { enabled = true },  -- use perlcritic and pass a non-default location for its config
--       syntax = { enabled = true }, -- enable syntax checking and use a non-default perl binary
--       -- perltidy = { perltidyrc = '/my/projects/.perltidyrc' } -- non-default location for perltidy's config
--     }
--   }
-- }
-- require("lvim.lsp.manager").setup("perlpls", pls_config)

-- Adiciona o eslint para melhor suporte a projetos js (mantém o tsserver)
require("lvim.lsp.manager").setup("eslint", {})

-- Emmet everywhere (hopefuly)
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local emmet_options = {
  capabilities = capabilities,
  filetypes = {
    "html",
    "typescript",
    "javascript",
    "javascriptreact",
    "xml",
  },
  root_dir = function ()
    return vim.loop.cwd()
  end
}
require("lvim.lsp.manager").setup("emmet_ls", emmet_options)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
vim.tbl_map(function(server)
  return server ~= "emmet_ls"
end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
lvim.lsp.on_attach_callback = function(client, bufnr)
  if client.name == "omnisharp" then
    client.server_capabilities.semanticTokensProvider = {
      full = vim.empty_dict(),
      legend = {
        tokenModifiers = { "static_symbol" },
        tokenTypes = {
          "comment",
          "excluded_code",
          "identifier",
          "keyword",
          "keyword_control",
          "number",
          "operator",
          "operator_overloaded",
          "preprocessor_keyword",
          "string",
          "whitespace",
          "text",
          "static_symbol",
          "preprocessor_text",
          "punctuation",
          "string_verbatim",
          "string_escape_character",
          "class_name",
          "delegate_name",
          "enum_name",
          "interface_name",
          "module_name",
          "struct_name",
          "type_parameter_name",
          "field_name",
          "enum_member_name",
          "constant_name",
          "local_name",
          "parameter_name",
          "method_name",
          "extension_method_name",
          "property_name",
          "event_name",
          "namespace_name",
          "label_name",
          "xml_doc_comment_attribute_name",
          "xml_doc_comment_attribute_quotes",
          "xml_doc_comment_attribute_value",
          "xml_doc_comment_cdata_section",
          "xml_doc_comment_comment",
          "xml_doc_comment_delimiter",
          "xml_doc_comment_entity_reference",
          "xml_doc_comment_name",
          "xml_doc_comment_processing_instruction",
          "xml_doc_comment_text",
          "xml_literal_attribute_name",
          "xml_literal_attribute_quotes",
          "xml_literal_attribute_value",
          "xml_literal_cdata_section",
          "xml_literal_comment",
          "xml_literal_delimiter",
          "xml_literal_embedded_expression",
          "xml_literal_entity_reference",
          "xml_literal_name",
          "xml_literal_processing_instruction",
          "xml_literal_text",
          "regex_comment",
          "regex_character_class",
          "regex_anchor",
          "regex_quantifier",
          "regex_grouping",
          "regex_alternation",
          "regex_text",
          "regex_self_escaped_character",
          "regex_other_escape",
        },
      },
      range = true,
    }
  end
end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
-- local formatters = require "lvim.lsp.null-ls.formatters"
-- formatters.setup {
--   { command = "black", filetypes = { "python" } },
--   { command = "isort", filetypes = { "python" } },
--   {
--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
-- }

-- -- set additional linters
-- local linters = require "lvim.lsp.null-ls.linters"
-- linters.setup {
--   { command = "flake8", filetypes = { "python" } },
--   {
--     -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "shellcheck",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--severity", "warning" },
--   },
--   {
--     command = "codespell",
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "javascript", "python" },
--   },
-- }

-- Additional Plugins
lvim.plugins = {
  -- {"yko/mojo.vim"},
  -- {
  --   "zbirenbaum/copilot-cmp",
  --   event = "InsertEnter",
  --   dependencies = { "zbirenbaum/copilot.lua" },
  --   config = function()
  --     vim.defer_fn(function()
  --       require("copilot").setup() -- https://github.com/zbirenbaum/copilot.lua/blob/master/README.md#setup-and-configuration
  --       require("copilot_cmp").setup() -- https://github.com/zbirenbaum/copilot-cmp/blob/master/README.md#configuration
  --     end, 100)
  --   end,
  -- },
  {
    "rebelot/kanagawa.nvim",
    config = function ()
      require("kanagawa").setup {
        dimInactive = true,
        globalStatus = true,
        theme = "wave",
        background = {
          dark = "wave",
          light = "lotus"
        },
        colors = {
            theme = {
                all = {
                    ui = {
                        bg_gutter = "none",
                        bg_visual = "#2D4F67"
                    },
                }
            }
        },
      }
    end
  },
  -- Neovim motions on speed!
  {
    "smoka7/hop.nvim",
    version = "*",
    event = "BufRead",
    config = function ()
      require("hop").setup {
        keys = 'tnplvmdhsefucwyxriao',
        multi_windows = false
      }
      vim.api.nvim_set_keymap("", "s", "<cmd>HopWord<cr>", { silent = true } )
      vim.api.nvim_set_keymap("", "h", "<cmd>HopChar2<cr>", { silent = true })
      vim.api.nvim_set_keymap("", 'L', "<cmd>HopLine<cr>", { silent = true })
      vim.api.nvim_set_keymap("", 'l', "<cmd>HopLineStart<cr>", { silent = true })
      vim.api.nvim_set_keymap("", 'f', "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.AFTER_CURSOR,current_line_only = true})<cr>", { silent = true })
      vim.api.nvim_set_keymap("", 'F', "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.BEFORE_CURSOR,current_line_only = true})<cr>", { silent = true })
      vim.api.nvim_set_keymap("", '<C-w>', "<cmd>HopWordCurrentLineAC<cr>", { silent = true })
      vim.api.nvim_set_keymap("", '<C-b>', "<cmd>HopWordCurrentLineBC<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "t", "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.AFTER_CURSOR,current_line_only = true,hint_offset = -1})<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "T", "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.BEFORE_CURSOR,current_line_only = true,hint_offset = 1})<cr>", { silent = true })
    end,
  },

  -- Better jump to line
  {
    "nacro90/numb.nvim",
    event = "BufRead",
    config = function ()
      require("numb").setup {
        show_numbers = true,
        show_cursorline = true,
      }
    end,
  },

  -- Colered parentheses like VSCode
  { "HiPhish/rainbow-delimiters.nvim" },

  -- Hint for function signatures when you type
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    config = function() require"lsp_signature".on_attach() end,
  },

  -- Preview markdown on neovim (requires Glow binary)
  {"ellisonleao/glow.nvim", config = true, cmd = "Glow"},

  -- Simple session management
  {
    "folke/persistence.nvim",
      event = "BufReadPre", -- this will only start session saving when an actual file was opened
      lazy = true,
      config = function()
        require("persistence").setup()
    end,
  },

  -- Replacement UI for messages, cmdline and the popupmenu
  ({
    "folke/noice.nvim",
    event = "VimEnter",
    config = function()
      require("noice").setup({
        lsp = {
          override = {
            ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
            ["vim.lsp.handlers"] = true,
            ["vim.lsp.util.stylize_markdown"] = true,
            ["cmp.entry.get_documentation"] = true,
          },
          hover = { enabled = false },
          signature = { enabled = false },
        }
      })
    end,
    dependencies = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      "rcarriga/nvim-notify",
      }
  }),
  -- Highlight TODOs
  {
    "folke/todo-comments.nvim",
    event = "BufRead",
    config = function ()
      require("todo-comments").setup()
    end
  },

  -- Mappings to delete, change and add surroundings
  -- { 'tpope/vim-surround' },
  {
    "roobert/surround-ui.nvim",
    dependencies = {
      "kylechui/nvim-surround",
      "folke/which-key.nvim",
    },
    config = function()
      require("surround-ui").setup({
        root_key = "i"
      })
    end,
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
        require("nvim-surround").setup({
            -- Configuration here, or leave empty to use defaults
        })
    end
  },

  -- Inline git blame
  { "f-person/git-blame.nvim" },

  -- Calls lazygit from Neovim (requires lazygit binary)
  { "kdheepak/lazygit.nvim" },
  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",         -- required
      "sindrets/diffview.nvim",        -- optional - Diff integration

      -- Only one of these is needed, not both.
      -- "nvim-telescope/telescope.nvim", -- optional
      -- "ibhagwan/fzf-lua",              -- optional
    },
    config = true
  },

  -- Color highlighter
  {
    "NvChad/nvim-colorizer.lua",
    config = function ()
      require("colorizer").setup({
        filetypes = { "css", "scss" },
        user_default_options = {
        RGB = true,
        RRGGBB = true,
        RRGGBBAA = true,
        rgb_fn = true,
        hsl_fn = true,
        css = true,
        css_fn = true
      }})
    end,
  },

  -- Multi cursor support
  {
    "mg979/vim-visual-multi",
    branch = "master",
  },

  -- -- Flutter and dart support
  -- {
  --   "akinsho/flutter-tools.nvim",
  --   config = function ()
  --     require("flutter-tools").setup{}
  --   end
  -- },

  -- Remembers last place on file
  {
    "ethanholz/nvim-lastplace",
    event = "BufRead",
    config = function()
      require("nvim-lastplace").setup({
        lastplace_ignore_buftype = { "quickfix", "nofile", "help" },
        lastplace_ignore_filetype = {
          "gitcommit", "gitrebase", "svn", "hgcommit",
        },
        lastplace_open_folds = true,
      })
    end,
  },

  -- Autoclose and autorename html tag
  {
    "windwp/nvim-ts-autotag",
    config = function ()
      require("nvim-ts-autotag").setup()
    end
  },

  -- Search and replace
  {
    "windwp/nvim-spectre",
    event = "BufRead",
    -- TODO: melhorar os keybindings desse plugin
    config = function()
      require("spectre").setup()
    end,
  },

  -- Prevent the cursor from moving when using shift and filter actions
  {
   "gbprod/stay-in-place.nvim",
    config = function()
      require("stay-in-place").setup()
    end
  },

  -- Scrollbars
  {
    "petertriho/nvim-scrollbar",
    config = function ()
      require("scrollbar").setup({
        marks = {
          GitAdd = {
              text = ".",
              highlight = "GitSignsAdd",
          },
          GitChange = {
              text = ".",
              highlight = "GitSignsChange",
          },
        }
      })
      require("scrollbar.handlers.gitsigns").setup()
    end
  },

  -- Better glance at matched information, with support to show on scrollbars.
  {
    "kevinhwang91/nvim-hlslens",
    config = function()
      require("scrollbar.handlers.search").setup()
    end
  },

  -- Funny animations with buffer content
  { "Eandrju/cellular-automaton.nvim" },

  -- Enable smooth scrolling
  {
    "karb94/neoscroll.nvim",
    config = function ()
      require("neoscroll").setup()
    end
  },
  {
    "iamcco/markdown-preview.nvim",
    build = "cd app && npm install",
    ft = "markdown",
    config = function()
      vim.g.mkdp_auto_start = 1
    end,
  },

  -- Use the w, e, b motions like a spider. Considers camelCase and skips insignificant punctuation
  {
    "chrisgrieser/nvim-spider",
    lazy = true,
    config = function ()
      require("spider").setup({
        skipInsignificantPunctuation = false
      })
    end
  },

  -- Detect current indent
  { "tpope/vim-sleuth" },

  -- Devcontainer support
  {
    url = "https://codeberg.org/esensar/nvim-dev-container",
    config = function ()
      require("devcontainer").setup({})
    end
  },
  {
    "aaronhallaert/advanced-git-search.nvim",
    config = function()
      require("telescope").setup({
        extensions = {
          advanced_git_search = {
            -- See Config
          }
        }
      })

      require("telescope").load_extension("advanced_git_search")
    end,
    dependencies = {
      {
        "nvim-telescope/telescope.nvim",
        "tpope/vim-fugitive",
        "tpope/vim-rhubarb",
      }
    },
  }
}

-- }
-- Autocommands
lvim.autocommands = {
  { "User", {
    pattern = { "TelescopePreviewerLoaded" },
    command = "setlocal number"
  } }
}
-- Better copy and paste on WSL (needs win32yank.exe binary)
if vim.fn.has "wsl" == 1 then
  vim.g.clipboard = {
    name = 'WslClipboard',
    copy = {
      ["+"] = "win32yank.exe -i --crlf",
      ["*"] = "win32yank.exe -i --crlf",
    },
    paste = {
      ["+"] = "win32yank.exe -o --lf",
      ["*"] = "win32yank.exe -o --lf",
    },
  }
end

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.perl = {
  install_info = {
    url = 'https://github.com/tree-sitter-perl/tree-sitter-perl',
    revision = 'release',
    files = { "src/parser.c", "src/scanner.c" },
  }
}
-- Load custom config files
require("dashboard")
